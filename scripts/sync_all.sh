#!/bin/bash

set -u # stop if a variable is not initialized
set -e # stop in case of error


TMP_DIR=$(mktemp -d -t sync-tools-XXXXXXXXXX)

git clone https://github.com/usegalaxy-eu/usegalaxy-eu-tools.git $TMP_DIR/usegalaxy-eu-tools

echo "------ Looking for updated tools ------"

cd files

for i in *yml; do
  echo "Fixing lockfile ${i}..."
  python3 $TMP_DIR/usegalaxy-eu-tools/scripts/fix-lockfile.py $i
done

for i in *yml; do
  echo "Updating ${i}..."
  python3 $TMP_DIR/usegalaxy-eu-tools/scripts/update-tool.py $i
done
